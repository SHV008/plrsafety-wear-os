package com.sathya.plrsmartwatchapp.utils;

/**
 * Created by rushil on 14/6/17.
 */

public class Constants {

    //live sever
    //public static final String BASE_URL = "http://108.161.131.151/salesapi2018oms/";
    public static final String BASE_URL = "http://myprojectdemo.org/plr_demo/";

    public static final int STATUS_SUCCESS = 200;

    public static final String BASE_64_ENCODED_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoFxtJhYyku74uIpfO7uaMHrEmUiCvVoHFO/MRfHLla4CYPsE/XYOK6EgFaEjxNuhGB5YzRuVZ+WuSrPRw56wAFuW3VTn2QPaiIX70mj8X3Cjp67cRNzPaeqM9hLNnvf+Mw4gXEApdXqjhj7MTc0ONB7K+sYYdgooiAt/5e+AP4iwIoYQVTJm4xwBlr06qmbdqsGpikYwyc6UAREH0O30h+hP3NDshiaY44n6stlUs9d4XMBhd1WfhcYDNskkHMirs0uVbngI8yZ0oDUGeUFAjHK0H1ljMpBWaFYGcCW86BvQ/27BiXCTaIPX45HM9+l0up0Nv4iGU74cxQKdR5NcLwIDAQAB";
    public static final int RES_CODE_CANCEL_PURCHASE=-1005;
    public static final int PAID_IMAGE=1;
    public static final int FREE_IMAGE=0;

    //    Date Format
    public static final String DATE_FORMAT_API = "dd-MM-yyyy HH:mm:ss";
    public static final String DATE_FORMAT_APP = "dd-MM-yyyy HH:mm:ss";

    public static final String DISTRICT_ID = "districtId";
    public static final String TALUKO_ID = "talukoId";

    public static final String IS_LOGIN_USER = "isLoginUser";

    public static final int REQ_CODE_CAPTURE_IMAGE = 10, REQ_CODE_GALLERY_IMAGE = 11;
    public static final int PERMISSION_REQ_CODE_CAMERA = 30,PERMISSION_REQ_CODE_GALLERY=31,PERMISSION_REQ_CODE_STORAGE=32;

    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;
    public static final int TYPE_FOOTER = 2;

    public static final String FCM_TOKEN_ID = "TestAndroidToken";

    public static final String DEVICE_TYPE = "Android";
    public static final String LOGIN_TYPE = "Type";

    public static final String SEND_MOBILE_NUMBER  = "+918511131807"; //64226577692

    public static final String IS_SEND_MESSAGE  = "is_send_message";
    public static final String COUNTRY_CODE  = "country_code";
    public static final String PRIMARY_EMERGENCY_NUMBER  = "primary_emergency_number";
    public static final String FULL_NAME  = "full_name";
    public static final String SECONDARY_EMERGENCY_NUMBER  = "secondary_emergency_number";
    public static final String PERSONAL_MOBILE_NUMBER  = "personal_emergency_number";
    public static final String EMAIL  = "email";
    public static final String PASSWORD  = "password";
    public static final String IS_LOGIN  = "is_login";
    public static final String IS_ADDED_TRIP_PLANNER  = "is_added_trip_planner";
    public static final String IS_PAID_USER  = "is_paid_user";

    public static final String AREA  = "area";
    public static final String ETD  = "etd";
    public static final String ETA  = "eta";
    public static final String PARTY_NUMBER  = "party_number";
    public static final String PARTY_MEMBER  = "party_member";
    public static final String MEDICAL_CONDITION  = "mediacal_condition";


    public static final String STORED_MESSAGE  = "stored_message";

//    Intent params
    public static final String EXTRA_CATEGORY_OBJ = "catObj";
    public static final String EXTRA_SUB_CATEGORY_OBJ = "subSatObj";
    public static final String EXTRA_ITEM_OBJ = "itemObj";

    //    API names and Params
    /*public static final String API_REGISTRATION = "host_signup";
    public static final int REQ_CODE_REGISTRATION = 100;
    public static final String API_LOGIN = "login";
    public static final int REQ_CODE_LOGIN= 101;
    public static final String API_LOGIN_SOCIAL = "login_social";
    public static final int REQ_CODE_LOGIN_SOCIAL = 1010;
    public static final String API_FORGOT_PASSWORD = "forgotPassword";
    public static final int REQ_CODE_FORGOT_PASSWORD= 102;
    public static final String API_ADVENTURE_LIST = "adventure_list";
    public static final int REQ_CODE_ADVENTURE_LIST= 103;
    public static final String API_EDIT_PROFILE = "edit_profile";
    public static final int REQ_CODE_EDIT_PROFILE= 104;
    public static final String API_POST_FEEDBACK = "feedback";
    public static final int REQ_CODE_POST_FEEDBACK= 105;
    public static final String API_ADD_ADVENTURE = "create_adventure";
    public static final int REQ_CODE_ADD_ADVENTURE= 106;
    public static final String API_GET_CLIENT_TOKEN = "braintree_token";
    public static final int REQ_CODE_GET_CLIENT_TOKEN = 107;
    public static final String API_MAKE_BT_TRANSACTION = "braintree_transaction";
    public static final int REQ_CODE_MAKE_BT_TRANSACTION = 108;
    public static final String API_CHECK_SEAT_AVAILABILITY = "check_seat_availibility";
    public static final int REQ_CODE_CHECK_SEAT_AVAILABILITY= 109;
    public static final String API_HISTORY_LIST = "user_booking_list";
    public static final int REQ_CODE_HISTORY_LIST= 110;
    public static final String API_UPDATE_ADVENTURE = "update_adventure";
    public static final int REQ_CODE_UPDATE_ADVENTURE= 116;
    public static final String API_UPLOAD_VEHICLE_IMAGE = "upload_vehicle_iamge";
    public static final int REQ_CODE_UPLOAD_VEHICLE_IMAGE= 120;
    public static final String API_UPLOAD_HOST_IMAGE = "upload_host_iamge";
    public static final int REQ_CODE_UPLOAD_HOST_IMAGE= 124;
    public static final String API_UPLOAD_REGISTRATION_IMAGE = "upload_registration_iamge";
    public static final int REQ_CODE_UPLOAD_REGISTRATION_IMAGE= 126;
    public static final String API_UPLOAD_INSURANCE_IMAGE = "upload_insurance_iamge";
    public static final int REQ_CODE_UPLOAD_INSURANCE_IMAGE= 128;*/

    public static final String API_GET_PACKAGES_LIST = "get_packages_list";
    public static final int REQ_CODE_GET_PACKAGES_LIST = 1004;

    public static final String API_GET_MY_COURSES_LIST = "get_my_courses_list";
    public static final int REQ_CODE_GET_MY_COURSES_LIST = 1044;

    public static final String API_ADD_MESSAGES_LIST = "add_messages_list";
    public static final int REQ_CODE_ADD_MESSAGES_LIST = 1016;

    public static final String API_GET_EXPENSES_LIST = "get_expenses_list";
    public static final int REQ_CODE_GET_EXPENSES_LIST = 1088;

    public static final String API_GET_EXPENSES_DETAILS_LIST = "get_expenses_details_list";
    public static final int REQ_CODE_GET_EXPENSES_DETAILS_LIST = 1090;

    public static final String API_GET_NOTIFICATIONS_LIST = "get_notifications_list";
    public static final int REQ_CODE_GET_NOTIFICATIONS_LIST = 1094;

    public static final String API_GET_DASH_DATA_LIST = "get_dash_data_list";
    public static final int REQ_CODE_GET_DASH_DATA_LIST = 1096;

    public static final String API_GET_PRODUCTS_LIST = "get_products_list";
    public static final int REQ_CODE_GET_PRODUCTS_LIST = 1098;

    public static final String API_POST_STOCK_RETURN_LIST = "post_stock_return";
    public static final int REQ_CODE_POST_STOCK_RETURN_LIST = 1888;

    public static final String API_POST_CLIENT_DATA = "post_client_data";
    public static final int REQ_CODE_POST_CLIENT_DATA = 1890;

    public static final String API_UPDATE_CLIENT_DATA = "update_client_data";
    public static final int REQ_CODE_UPDATE_CLIENT_DATA = 1896;

    public static final int REQ_CODE_GET_EVENT_LIST = 1027;

    public static final String API_POST_DISTRIBUTOR_DATA_LTB = "post_distributor_data_ltb";
    public static final int REQ_CODE_POST_DISTRIBUTOR_DATA_LTB = 1820;

    public static final String API_ADD_CREDIT = "addCredit";
    public static final int REQ_CODE_ADD_CREDIT = 1188;

    public static final String API_GET_VIDEOS_LIST = "getVideosList";
    public static final int REQ_CODE_GET_VIDEOS_LIST = 1010;

    public static final int REQ_CODE_GET_POTENTIAL = 1044;

    public static final String API_LOGIN = "login";
    public static final int REQ_CODE_LOGIN= 1018;

    public static final String API_SIGN_UP = "sign_up";
    public static final int REQ_CODE_SIGN_UP= 1020;

    public static final String API_ADD_TRIP_PLANNER = "trip_planner";
    public static final int REQ_CODE_ADD_TRIP_PLANNER= 1898;

    public static final String API_UPDATE_PROFILE = "update_profile";
    public static final int REQ_CODE_UPDATE_PROFILE= 1848;

    public static final String API_GET_COUNTRY_LIST = "get_country_list";
    public static final int REQ_CODE_GET_COUNTRY_LIST = 1646;

    public static final String LOGIN_DATA = "login_data";
    public static final String USER_ID = "userId";
    public static final String ACCESS_TOKEN = "access_token";

    public static final String API_GET_USER_STATE = "get_uset_state";
    public static final int REQ_CODE_GET_USER_STATE = 1086;

    public static final String API_POST_PAYMENT_DETAILS = "post_payment_details";
    public static final int REQ_CODE_POST_PAYMENT_DETAILS = 4816;
}
