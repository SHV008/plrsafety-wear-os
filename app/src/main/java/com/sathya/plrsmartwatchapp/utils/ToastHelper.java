package com.sathya.plrsmartwatchapp.utils;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.NonNull;


public class ToastHelper {
    private static Toast toast = null;
    private static ToastHelper toastHelper;

    public static ToastHelper getInstance() {
        if (toastHelper == null)
            toastHelper = new ToastHelper();
        return toastHelper;
    }


    public void showToast(@NonNull Context context, String message) {

        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void showToast(@NonNull Context context, String message, int duration) {

        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, message, duration);
        toast.show();
    }


    /*public void showToast(@NonNull Context context, String message, int duration, int gravity) {

        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, message, duration);
        toast.setGravity(gravity, 0, 0);
        toast.show();
    }*/
}