package com.sathya.plrsmartwatchapp.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by ${Janak} on 24-May-18.
 */
public class Utils {
    public static String getDay(Long time) {
        return new SimpleDateFormat("dd").format(new Date(time));
    }

    public static String getMonthYear(Long time) {
        return new SimpleDateFormat("MMMM yyyy").format(new Date(time));
    }

    public static String getWeekday(Long time) {
        return new SimpleDateFormat("EEEE").format(new Date(time));
    }

    public static String getFullDate(Long time) {
        return new SimpleDateFormat("dd-MM-yyyy").format(new Date(time));
    }

    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 100);
        return noOfColumns;
    }

    public static boolean isConnectingToInternet(Context context) {
        try {
            boolean isConnected = false;
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager != null) {
                NetworkInfo[] networkInfo = connectivityManager.getAllNetworkInfo();
                if (networkInfo != null)
                    for (int i = 0; i < networkInfo.length; i++)
                        if (networkInfo[i].getState() == NetworkInfo.State.CONNECTED) {
                            isConnected = true;
                            break;
                        }
            }
            Log.i("Connection", "" + isConnected);
            return isConnected;
        } catch (Exception e) {
            return false;
        }
    }

    public static void getGradient(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = activity.getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
    }

    private static Utils utils;
    public static Utils getInstance() {
        if (utils == null)
            utils = new Utils();
        return utils;
    }

    public boolean isEmailValid(String email) {
//        String expression = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        String expression = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        CharSequence inputStr = email;
        boolean flag = false;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            flag = true;
        }
        return flag;
    }

//    public static BitmapImageViewTarget getRoundedImageTarget(@NonNull final Context context, @NonNull final ImageView imageView,
//                                                              final float radius) {
//        return new BitmapImageViewTarget(imageView) {
//            @Override
//            protected void setResource(final Bitmap resource) {
//                RoundedBitmapDrawable circularBitmapDrawable =
//                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
//                circularBitmapDrawable.setCornerRadius(radius);
//                imageView.setImageDrawable(circularBitmapDrawable);
//            }
//        };
//    }

    public boolean checkInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        // test for connection
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            Log.v("", "Internet Connection Not Available");
            //Toast.makeText(context, context.getResources().getString(R.string.network_connectivity_not_available), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public static boolean isValidEmaillId(String email){

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    public String generateHashKey(Context context) {
        String hashKey = "";
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                hashKey = Base64.encodeToString(md.digest(), Base64.DEFAULT);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hashKey;
    }

    @SuppressLint("NewApi")
    public static boolean validateString(String object) {
        boolean flag = false;
        if (object != null && !object.equalsIgnoreCase("null") && !object.equalsIgnoreCase("(null)") &&
                !object.isEmpty() && !object.equalsIgnoreCase("(null)")) {
            flag = true;
        }
        return flag;
    }

    public void hideKeyboard(View v, Context context) {
        InputMethodManager mgr = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void showKeyboard(View v, Context context) {		/*		 * InputMethodManager mgr =		 * (InputMethodManager)context.getSystemService(		 * Context.INPUT_METHOD_SERVICE); mgr.showSoftInput(v, 0);		 */
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(v.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
    }

    /**
     * Function is used to hide keyboard while user click outside of keyboard on screen.
     * @param view  contains parent view id to get all child views.
     */
    @SuppressLint("ClickableViewAccessibility")
    public void setupOutSideTouchHideKeyboard(final View view)
    {
        if(view==null)
            return;
        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText))
        {
            view.setOnTouchListener(new View.OnTouchListener()
            {

                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    hideKeyboard(view,view.getContext());
                    return false;
                }

            });
        }

        // If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup)
        {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++)
            {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupOutSideTouchHideKeyboard(innerView);
            }
        }
    }

    public int[] getScreenWidthHeight(Context context)
    {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

		/*Display display = wm.getDefaultDisplay();
		int width = display.getWidth(); // deprecated
		int height = display.getHeight(); // deprecated*/

        DisplayMetrics displaymetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;
        int screenHeight = displaymetrics.heightPixels;
        int[] wh = { screenWidth, screenHeight };
//		int[] wh = { width, height };
        return wh;
    }

    public float getFileSizeInMb(File file)
    {
        try{
            if(!file.exists())
                return 0;
            // Get length of file in bytes
            long fileSizeInBytes = file.length();
            // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
            float fileSizeInKB = fileSizeInBytes / 1024;
            // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
            float fileSizeInMB = fileSizeInKB / 1024;
            return fileSizeInMB;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return 0;
    }

    /**Return image path of given URI*/
    public String getRealPathFromURI(Context context, Uri contentURI) {
//        Cursor cursor = getActivity().getContentResolver().query(contentURI, null, null, null, null);
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query(contentURI, projection, null, null, null);
        //Some of gallery application is return null
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(projection[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            return picturePath;
        }
    }

    /**It will encode the unicode characters properly before sending to web server.*/
   /* public String escapeJava(String text)
    {
        if(text!=null && text.length()>0)
            return StringEscapeUtils.escapeJava(text);
        return "";
    }

    *//**it will decode the unicode characters properly after receiving the response from web server.*//*
    public String unescapeJava(String text)
    {
        if(text!=null && text.length()>0)
            return StringEscapeUtils.unescapeJava(text);
        return "";
    }*/

    /**Return formatted date of passed output format*/
    public String getFormattedDate(String date, String input, String output) {
        try {
            SimpleDateFormat sdfInput = new SimpleDateFormat(input);
            SimpleDateFormat sdfOutput = new SimpleDateFormat(output);

            return sdfOutput.format(sdfInput.parse(date));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    /*public String getDateDifference(Context context,String date) {
        Date thenDate = covertStringToDate(date);
        //getFormattedDate(thenDate);
        String displayDate = Utils.getInstance().getFormattedDate(date);

        Calendar now = Calendar.getInstance();
        Calendar then = Calendar.getInstance();
        now.setTime(new Date());
        then.setTime(thenDate);

        // Get the represented date in milliseconds
        long nowMs = now.getTimeInMillis();
        long thenMs = then.getTimeInMillis();
        // Calculate difference in milliseconds
        long diff = nowMs - thenMs;

        // Calculate difference in seconds
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000);
        long diffHours = diff / (60 * 60 * 1000);
        long diffDays = diff / (24 * 60 * 60 * 1000);

        if (diffMinutes < 60) {
            if (diffMinutes < 0)
                return displayDate;
            else if (diffMinutes == 0)
                return context.getResources().getString(R.string.just_now);

            return diffMinutes + " " + context.getResources().getQuantityString(R.plurals.minuteAgo, (int) diffMinutes);
            *//*if (diffMinutes == 1)
                return diffMinutes + " " + mContext.getResources().getString(R.string.minute_ago);
            else
                return diffMinutes + " " + mContext.getResources().getString(R.string.minutes_ago);*//*

        } else if (diffHours < 24) {
            *//*if (diffHours == 1)
                return diffHours + " " + mContext.getResources().getString(R.string.hour_ago);
            else
                return diffHours + " " + mContext.getResources().getString(R.string.hours_ago);*//*

            return diffHours + " " + context.getResources().getQuantityString(R.plurals.hourAgo, (int) diffHours);

        } else if (diffDays < 30) {
            *//*if (diffDays == 1)
                return diffDays + " " + mContext.getResources().getString(R.string.day_ago);
            else
                return diffDays + " " + mContext.getResources().getString(R.string.days_ago);*//*

            return diffDays + " " + context.getResources().getQuantityString(R.plurals.dayAgo, (int) diffDays);
        } else {
            return displayDate;
        }
    }*/

    public Date covertStringToDate(String strDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = null;

        try {
            date = formatter.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public String longToDateString(long milliSecond)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSecond);
        return formatter.format(calendar.getTime());
    }

//    public void showSnackBar(View view, String message)
//    {
//        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
//        //snackbar.show();
//        View snackbarView = snackbar.getView();
//
//        ViewGroup.LayoutParams layoutParams=snackbarView.getLayoutParams();
//
//        if(layoutParams instanceof CoordinatorLayout.LayoutParams)
//        {
//            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) layoutParams;
//            params.gravity = Gravity.TOP;
//            layoutParams=params;
//        }
//        else if(layoutParams instanceof FrameLayout.LayoutParams)
//        {
//            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) layoutParams;
//            params.gravity = Gravity.TOP;
//            params.topMargin=getActionBarHeight(view.getContext());
//            layoutParams=params;
//        }
//        snackbarView.setLayoutParams(layoutParams);
//        snackbar.show();
//    }

    private int getActionBarHeight(Context context) {
        int actionBarHeight = 0;
        final TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
        return actionBarHeight;
    }

    /**Check String null and empty*/
    public boolean isEmptyString(String str)
    {
        if(str!=null && str.length()>0)
            return false;
        return true;
    }

    public void setImmersiveModeEnable(View view, boolean enable)
    {
        if(enable)
        {
            if(Build.VERSION.SDK_INT < 19){
                view.setSystemUiVisibility(View.GONE);
            } else {
                int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                view.setSystemUiVisibility(uiOptions);
            }
        }
        else
        {
            view.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        }
    }

    @SuppressLint("NewApi")
    public static String getFilePath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static void writeFile(Context context){
        try {
            OutputStream os = getLogStream(context);
            os.write(getInformation(context).getBytes("utf-8"));
            os.flush();
            os.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void writeFile(Context context, String str){
        try {
            OutputStream os = getLogStream(context);
            os.write(str.getBytes("utf-8"));
            os.flush();
            os.close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public static OutputStream getLogStream(Context context) throws IOException {
        //crash_log_pkgname.log
        String model = Build.MODEL.replace(" ", "_");
        String fileName = String.format("compress_"+ model + ".log", context.getPackageName());
        File file  = new File(Environment.getExternalStorageDirectory(), fileName);

        if(!file.exists()){
            file.createNewFile();
        }

        return new FileOutputStream(file, true);
    }

    public static String getInformation(Context context){
        long current = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder().append('\n');
        sb.append("BOARD: ").append(Build.BOARD).append('\n');
        sb.append("BOOTLOADER: ").append(Build.BOOTLOADER).append('\n');
        sb.append("BRAND: ").append(Build.BRAND).append('\n');
        sb.append("CPU_ABI: ").append(Build.CPU_ABI).append('\n');
        sb.append("CPU_ABI2: ").append(Build.CPU_ABI2).append('\n');
        sb.append("DEVICE: ").append(Build.DEVICE).append('\n');
        sb.append("DISPLAY: ").append(Build.DISPLAY).append('\n');
        sb.append("FINGERPRINT: ").append(Build.FINGERPRINT).append('\n');
        sb.append("HARDWARE: ").append(Build.HARDWARE).append('\n');
        sb.append("HOST: ").append(Build.HOST).append('\n');
        sb.append("ID: ").append(Build.ID).append('\n');
        sb.append("MANUFACTURER: ").append(Build.MANUFACTURER).append('\n');
        sb.append("MODEL: ").append(Build.MODEL).append('\n');
        sb.append("PRODUCT: ").append(Build.PRODUCT).append('\n');
        sb.append("SERIAL: ").append(Build.SERIAL).append('\n');
        sb.append("TAGS: ").append(Build.TAGS).append('\n');
        sb.append("TIME: ").append(Build.TIME).append(' ').append(toDateString(Build.TIME)).append('\n');
        sb.append("TYPE: ").append(Build.TYPE).append('\n');
        sb.append("USER: ").append(Build.USER).append('\n');
        sb.append("VERSION.CODENAME: ").append(Build.VERSION.CODENAME).append('\n');
        sb.append("VERSION.INCREMENTAL: ").append(Build.VERSION.INCREMENTAL).append('\n');
        sb.append("VERSION.RELEASE: ").append(Build.VERSION.RELEASE).append('\n');
        sb.append("VERSION.SDK_INT: ").append(Build.VERSION.SDK_INT).append('\n');
        sb.append("LANG: ").append(context.getResources().getConfiguration().locale.getLanguage()).append('\n');
        sb.append("APP.VERSION.NAME: ").append(getVersionName(context)).append('\n');
        sb.append("APP.VERSION.CODE: ").append(getVersionCode(context)).append('\n');
        sb.append("CURRENT: ").append(current).append(' ').append(toDateString(current)).append('\n');

        return sb.toString();
    }

    public static String toDateString(long timeMilli){
        Calendar calc = Calendar.getInstance();
        calc.setTimeInMillis(timeMilli);
        return String.format(Locale.CHINESE, "%04d.%02d.%02d %02d:%02d:%02d:%03d",
                calc.get(Calendar.YEAR), calc.get(Calendar.MONTH) + 1, calc.get(Calendar.DAY_OF_MONTH),
                calc.get(Calendar.HOUR_OF_DAY), calc.get(Calendar.MINUTE), calc.get(Calendar.SECOND), calc.get(Calendar.MILLISECOND));
    }

    public static String getVersionName(Context context){
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packInfo = null;
        String version = null;
        try {
            packInfo = packageManager.getPackageInfo(context.getPackageName(),0);
            version = packInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return version;
    }

    public static int getVersionCode(Context context){
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packInfo = null;
        int version = 0;
        try {
            packInfo = packageManager.getPackageInfo(context.getPackageName(),0);
            version = packInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return version;
    }

}
