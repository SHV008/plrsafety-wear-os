package com.sathya.plrsmartwatchapp.utils;

import android.app.ProgressDialog;
import android.content.Context;

import androidx.annotation.NonNull;

import com.sathya.plrsmartwatchapp.R;

public class CustomDialog {
    private static CustomDialog customDialog;
    ProgressDialog progressDialog;

    public static CustomDialog getInstance() {
        if (customDialog == null) {
            customDialog = new CustomDialog();
        }
        return customDialog;
    }

    /**
     * Show progress dialog without message, Before show new dialog it is hide previous showing dialog
     *
     * @param context    Context of activity.
     * @param cancelable true: Hide out side touch or back press, false: Can not hide out side touch
     */
    public void show(@NonNull Context context, boolean cancelable) {
        try {
            //Before show new dialog hide previous showing dialog
            hide();

            progressDialog = new ProgressDialog(context);
            progressDialog.setCancelable(cancelable);
            progressDialog.setMessage(context.getString(R.string.msgLoading));
            progressDialog.show();
            /*if (progressDialog != null) {
                if (!progressDialog.isShowing()) {
                    progressDialog.show();
                }
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Show progress dialog without message, Before show new dialog it is hide previous showing dialog
     *
     * @param context    Context of activity.
     * @param message    Message of dialog.
     * @param cancelable true: Hide out side touch or back press, false: Can not hide out side touch
     */
    public void show(@NonNull Context context, @NonNull String message, boolean cancelable) {

        try {

            //Before show new dialog hide previous showing dialog
            hide();
            //Set default dialog message "Loading"
            message = (message != null && message.trim().length() > 0) ? message : context.getString(R.string.msgLoading);
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(cancelable);

            progressDialog.show();


            /*if (progressDialog != null) {
                if (!progressDialog.isShowing()) {
                    progressDialog.show();
                }
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void hide() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public boolean isDialogShowing() {
        if (progressDialog != null)
            return progressDialog.isShowing();
        else
            return false;
    }

    //Show alert dialog
    /*public void showMessageDialog(Context context, String title, String message) {
       *//* if(title == null && title.equalsIgnoreCase("")){
            title = "Eppico";
        }*//*
        
        if (message != null && message.trim().length() > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            //builder.setTitle(title);
            builder.setCancelable(true);
            builder.setMessage(message);
            builder.setPositiveButton(context.getString(R.string.btnOk),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            // create alert dialog
            AlertDialog alertDialog = builder.create();
            // show it
            alertDialog.show();
        }
    }*/

    /*//Show alert dialog
    public void showMessageDialog(Context context, String message, @Nullable View.OnClickListener clickListener) {
        if (message != null && message.trim().length() > 0) {

            final Dialog dialog = new Dialog(context, R.style.DialogTheme);
            dialog.setContentView(R.layout.dialog_layout);

            TextView txtMessage = (TextView) dialog.findViewById(R.id.txtMessage);
            Button btnDialogOk = (Button) dialog.findViewById(R.id.btnDialogOk);
            txtMessage.setText(message);

            if (clickListener == null) {
                clickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                };
            }
            btnDialogOk.setOnClickListener(clickListener);

            txtMessage.setOnClickListener(clickListener);

            *//** Set Dialog width match parent *//*
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.show();
        }
    }

    //Show alert dialog
    public void showWebviewDialog(final Context context, String url, @Nullable View.OnClickListener clickListener) {
        if (url != null && url.trim().length() > 0) {
            if (url != null && url.trim().length() > 0) {
                final Dialog dialog = new Dialog(context, R.style.DialogTheme);
                dialog.setContentView(R.layout.dialog_webview_layout);

                WebView webView = (WebView) dialog.findViewById(R.id.webView);
                Button btnDialogClose = (Button) dialog.findViewById(R.id.btnDialogClose);

                webView.getSettings().setJavaScriptEnabled(true); // enable javascript

                webView.setWebViewClient(new WebViewClient() {
                    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                        Toast.makeText(context, description, Toast.LENGTH_SHORT).show();

                    }
                });

                webView.loadUrl(url);

                if (clickListener == null) {
                    clickListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    };
                }
                btnDialogClose.setOnClickListener(clickListener);

                // Set Dialog width match parent
                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                dialog.show();
            }
        }
    }*/
}
