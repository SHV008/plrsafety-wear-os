package com.sathya.plrsmartwatchapp;

/**
 * Created by Dilavar on 8/31/2018.
 */

public interface PermissionResult {

    void permissionGranted();

    void permissionDenied();

    void permissionForeverDenied();
}
