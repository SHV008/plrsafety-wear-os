package com.sathya.plrsmartwatchapp.apis;

import com.sathya.plrsmartwatchapp.models.GetUserStateRes;
import com.sathya.plrsmartwatchapp.models.LoginReq;
import com.sathya.plrsmartwatchapp.models.LoginRes;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface SalesAppApi {
    @POST("public/api/login")
    Call<LoginRes> login(@Body LoginReq loginReq);

    @GET("public/api/user/state")
    Call<GetUserStateRes> getUserState();


    /*@POST("public/api/register")
    Call<SignUpRes> register(@Body SignUpReq signUpReq);

    @POST("public/api/trip_planner")
    Call<TripPlannerRes> tripPlanner(@Body TripPlannerReq signUpReq);

    @POST("public/api/user/update")
    Call<UpdateProfileRes> updateProfile(@Body UpdateProfileReq signUpReq);

    @POST("public/api/user/package")
    Call<PostPaymentDataRes> postPaymentDetails(@Body PostPaymentDataReq postPaymentDataReq);

    @GET("public/api/countries")
    Call<CountryListRes> getCountryList();

    @GET("public/api/packages")
    Call<PackageListRes> getPackageList();*/
}