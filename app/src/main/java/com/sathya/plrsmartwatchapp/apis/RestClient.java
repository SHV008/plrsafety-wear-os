package com.sathya.plrsmartwatchapp.apis;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sathya.plrsmartwatchapp.BuildConfig;
import com.sathya.plrsmartwatchapp.R;
import com.sathya.plrsmartwatchapp.utils.Constants;
import com.sathya.plrsmartwatchapp.utils.CustomDialog;
import com.sathya.plrsmartwatchapp.utils.PrefHelper;
import com.sathya.plrsmartwatchapp.utils.ToastHelper;
import com.sathya.plrsmartwatchapp.utils.Utils;

import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static String TAG = RestClient.class.getName();
    private static SalesAppApi API_CLIENT;
    static Retrofit retrofit;
    private Context context;
    private static int TIME_OUT_DURATION = 300; // Seconds

    public RestClient(Context context, String authKey) {
        this.context = context;
        setupRestClient(authKey);
    }

    public static SalesAppApi getApiClient() {
        return API_CLIENT;
    }

    public void setupRestClient(final String authKey) {
        OkHttpClient.Builder builderOkHttp = new OkHttpClient.Builder();
        builderOkHttp.connectTimeout(TIME_OUT_DURATION, TimeUnit.SECONDS);
        builderOkHttp.readTimeout(TIME_OUT_DURATION, TimeUnit.SECONDS);
        builderOkHttp.writeTimeout(TIME_OUT_DURATION, TimeUnit.SECONDS);
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builderOkHttp.addInterceptor(httpLoggingInterceptor);
        //builderOkHttp.networkInterceptors().add(httpLoggingInterceptor);
//      builderOkHttp.hostnameVerifier(getHostNameVerifier());

        builderOkHttp.networkInterceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Log.v("AccessToken : ",""+PrefHelper.getString(Constants.ACCESS_TOKEN, ""));
                Request.Builder requestBuilder = chain.request().newBuilder();
                requestBuilder.addHeader("Content-Type", "application/json");
                requestBuilder.header("Content-Type", "application/json");
                requestBuilder.addHeader("Authorization", "Bearer "+ PrefHelper.getString(Constants.ACCESS_TOKEN, ""));
                requestBuilder.header("Authorization", "Bearer "+PrefHelper.getString(Constants.ACCESS_TOKEN, ""));

                return chain.proceed(requestBuilder.build());
            }
        });


        OkHttpClient client = builderOkHttp.build();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        API_CLIENT = retrofit.create(SalesAppApi.class);
    }

    /**Return default time zone. e.g "Asia/Calcutta" */
    public String getTimeZone()
    {
        /*TimeZone tz = TimeZone.getDefault();
        MyLog.v("SplashScreenActivity","TimeZone   "+tz.getDisplayName(false, TimeZone.SHORT)+" Timezon id :: " +tz.getID());*/
        String timeZone= TimeZone.getDefault().getID();
        MyLog.v(TAG,"TimeZone: " +timeZone);

        return timeZone;
    }

    public static Retrofit retrofit() {
        return retrofit;
    }

    @SuppressWarnings("unchecked")
    public void makeApiRequest(final Context activity, Object call, final ApiResponseListener apiResponseListener, final int reqCode, boolean showProgressDialog) {
        try {
            if (Utils.getInstance().checkInternetConnection(activity)) {
                if (showProgressDialog)
                    CustomDialog.getInstance().show(activity, false);

                final Call<Object> objectCall = (Call<Object>) call;
                objectCall.enqueue(new Callback<Object>() {
                    @Override
                    public void onResponse(Call<Object> call, retrofit2.Response<Object> response) {
                        if (shouldCallCallback(apiResponseListener)) {
                            CustomDialog.getInstance().hide();
                        }
                        /** Returns true if code() is in the range [200..300). */
                        if (response.isSuccessful()) {
                            MyLog.v(TAG, "onResponse - Success : " + response.body());

                            if (shouldCallCallback(apiResponseListener))
                                apiResponseListener.onApiResponse(call, response, reqCode);

                           /* if(apiResponseListener instanceof Fragment) {
                                Fragment fragment  = (Fragment) apiResponseListener;
                                if(fragment.isAdded()){
                                    apiResponseListener.onApiResponse(call, response, reqCode);
                                }
                            }*/
                        } else {

                            String errorMessage = getErrorMessage(response);
                            if (shouldCallCallback(apiResponseListener)) {
                                if (response.code() == HttpURLConnection.HTTP_NOT_FOUND)
                                    apiResponseListener.onApiError(call, errorMessage, reqCode);
                                else
                                    apiResponseListener.onApiError(call, errorMessage, reqCode);
                            }

                            /*Note: This error log is only for developer. Where developer identify exact API error*/
                            if (BuildConfig.DEBUG) {
                                MyLog.e(TAG, "error message: " + errorMessage);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        //there is more than just a failing request (like: no internet connection)
                        MyLog.e(TAG, "onFailure - Fail : " + t.getMessage());
                        CustomDialog.getInstance().hide();

                        if (/*t instanceof ConnectException || */t instanceof SocketTimeoutException) {
                            //Displaying no network connection error
                            if (shouldCallCallback(apiResponseListener))
                                apiResponseListener.onApiError(call, activity.getString(R.string.msgInternetConnectionNotAvailable), reqCode);
                        } else {
                            if (shouldCallCallback(apiResponseListener))
                                apiResponseListener.onApiError(call, activity.getString(R.string.msgProblemWithRequest), reqCode);
                        }
                    }
                });
            } else {
                CustomDialog.getInstance().hide();
                ToastHelper.getInstance().showToast(activity, activity.getString(R.string.msgInternetConnectionNotAvailable));
            }
        } catch (Exception e) {
            CustomDialog.getInstance().hide();
            e.printStackTrace();
            ToastHelper.getInstance().showToast(activity, activity.getString(R.string.msgProblemWithRequest));
        }
    }

    @SuppressWarnings("unchecked")
    public void makeApiRequest(final Activity activity, Object call, final ApiResponseListener apiResponseListener, final int reqCode, final int position, boolean showProgressDialog) {
        try {
            if (Utils.getInstance().checkInternetConnection(activity)) {
                if (showProgressDialog)
                    CustomDialog.getInstance().show(activity, false);

                final Call<Object> objectCall = (Call<Object>) call;
                objectCall.enqueue(new Callback<Object>() {

                    @Override
                    public void onResponse(Call<Object> call, retrofit2.Response<Object> response) {
                        CustomDialog.getInstance().hide();
                        /** Returns true if code() is in the range [200..300).*/
                        if (response.isSuccessful()) {
                            MyLog.v(TAG, "onResponse - Success : " + response.body());
                            if (shouldCallCallback(apiResponseListener))
                                apiResponseListener.onApiResponse(call, response, reqCode, position);

                        } else {

                            String errorMessage = getErrorMessage(response);
                            if (shouldCallCallback(apiResponseListener)) {
                                if (response.code() == HttpURLConnection.HTTP_NOT_FOUND)
                                    apiResponseListener.onDataNotFound(call, errorMessage, reqCode);
                                else
                                    apiResponseListener.onApiError(call, errorMessage, reqCode);
                            }

                            /*Note: This error log is only for developer. Where developer identify exact API error*/
                            if (BuildConfig.DEBUG) {
                                MyLog.e(TAG, "error message: " + errorMessage);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Object> call, Throwable t) {
                        //there is more than just a failing request (like: no internet connection)

                        MyLog.e(TAG, "onFailure - Fail : " + t.getMessage());
                        CustomDialog.getInstance().hide();

                        if (t instanceof ConnectException) {
                            //Displaying no network connection error
                            if (shouldCallCallback(apiResponseListener))
                                apiResponseListener.onApiError(call, activity.getString(R.string.msgInternetConnectionNotAvailable), reqCode);
                        } else {
                            if (shouldCallCallback(apiResponseListener))
                                apiResponseListener.onApiError(call, activity.getString(R.string.msgProblemWithRequest), reqCode);
                        }
                    }

                });
            } else {
                CustomDialog.getInstance().hide();
                ToastHelper.getInstance().showToast(activity, activity.getString(R.string.msgInternetConnectionNotAvailable));
            }
        } catch (Exception e) {
            CustomDialog.getInstance().hide();
            e.printStackTrace();
            ToastHelper.getInstance().showToast(activity, activity.getString(R.string.msgProblemWithRequest));
        }
    }

    /**
     * Return Error message.
     * If error code is 404 then return error message which comes in error body.
     * For other ocde just return name of error(e.g Unauthorization, Bad Request)
     */
    private String getErrorMessage(retrofit2.Response<Object> response) {
        String errorMessage = "";
        try {


            if (response.code() == HttpURLConnection.HTTP_NOT_FOUND) {
                APIError error = ErrorUtils.parseError(response);
                if (error != null)
                    errorMessage = error.message();

               /*ResponseBody responseBody=response.errorBody();
                if (responseBody != null && responseBody.string().length()>0)
                    errorMessage = responseBody.string();*/
            }
            if (errorMessage == null || errorMessage.length() <= 0)
                errorMessage = response.message(); // It returns just name of error (e.g e.g Unauthorization, Bad Request ... etc)

        } catch (Exception e) {
            e.printStackTrace();
        }
        return errorMessage;
    }


    private boolean shouldCallCallback(ApiResponseListener apiResponseListener) {
        if (apiResponseListener instanceof Fragment) {
            Fragment fragment = (Fragment) apiResponseListener;
            if (fragment.isAdded())
                return true;
        } else if (apiResponseListener instanceof Activity) {
            Activity activity = (Activity) apiResponseListener;
            if (!activity.isFinishing())
                return true;
        }
        return false;
    }

    /*
    ****Show progress on api request*****
    private static class ProgressResponseBody extends ResponseBody {

        private final ResponseBody responseBody;
       // private final ProgressListener progressListener;
        private BufferedSource bufferedSource;

        public ProgressResponseBody(ResponseBody responseBody*//*, ProgressListener progressListener*//*) {
            this.responseBody = responseBody;
//            this.progressListener = progressListener;
        }

        @Override public MediaType contentType() {
            return responseBody.contentType();
        }

        @Override public long contentLength() {
            return responseBody.contentLength();
        }

        @Override public BufferedSource source() {
            if (bufferedSource == null) {
                bufferedSource = Okio.buffer(source(responseBody.source()));
            }
            return bufferedSource;
        }

        private Source source(Source source) {
            return new ForwardingSource(source) {
                long totalBytesRead = 0L;

                @Override public long read(Buffer sink, long byteCount) throws IOException {
                    long bytesRead = super.read(sink, byteCount);
                    // read() returns the number of bytes read, or -1 if this source is exhausted.
                    totalBytesRead += bytesRead != -1 ? bytesRead : 0;
                    long contentLength=responseBody.contentLength();
                    float percent=(totalBytesRead * 100)/contentLength;
                    MyLog.e(TAG,"API percentage: " + percent);

                  //  progressListener.update(totalBytesRead, responseBody.contentLength(), bytesRead == -1);
                    return bytesRead;
                }
            };
        }
    }*/
}