package com.sathya.plrsmartwatchapp;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import com.sathya.plrsmartwatchapp.apis.RestClient;

public class IdealDistributorApp extends Application {

    public static final String TAG = IdealDistributorApp.class
            .getSimpleName();

    public static SharedPreferences myPref;

    RestClient restClient;
    private static IdealDistributorApp mInstance;

    public static boolean DISABLE_FRAGMENT_ANIMATION=false;

    public static final String NOTIFICATION_CHANNEL_ID_SERVICE = "com.security.plr.service";
    public static final String NOTIFICATION_CHANNEL_ID_TASK = "com.security.plr_info";
    public static final String NOTIFICATION_CHANNEL_ID_INFO = "com.security.plr_new";

    @Override
    public void onCreate() {
        super.onCreate();
        //Fabric.with(this, new Crashlytics());
        mInstance = this;
        restClient = new RestClient(this, "");
        myPref = PreferenceManager.getDefaultSharedPreferences(IdealDistributorApp.mInstance);
    }

    public static synchronized IdealDistributorApp getInstance() {
        return mInstance;
    }

    public RestClient getRestClient()
    {
        return restClient;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.i("App kill", "Terminate");
    }

    public void initChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nm.createNotificationChannel(new NotificationChannel(NOTIFICATION_CHANNEL_ID_SERVICE, "App Service", NotificationManager.IMPORTANCE_DEFAULT));
            nm.createNotificationChannel(new NotificationChannel(NOTIFICATION_CHANNEL_ID_INFO, "Download Info", NotificationManager.IMPORTANCE_DEFAULT));
        }
    }
}