package com.sathya.plrsmartwatchapp.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.wearable.Wearable;
import com.sathya.plrsmartwatchapp.R;
import com.sathya.plrsmartwatchapp.utils.Constants;
import com.sathya.plrsmartwatchapp.utils.PrefHelper;

public class SplashActivity extends WearableActivity {

    private Handler handler;
    private Runnable starter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (PrefHelper.getBoolean(Constants.IS_LOGIN, false)) {
                    startActivity(new Intent(SplashActivity.this, DashActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(SplashActivity.this, SignInActivity.class));
                    finish();
                }
            }
        }, 3000);

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i ("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.i ("isMyServiceRunning?", false+"");
        return false;
    }

    @Override
    protected void onDestroy() {
        Log.i("MAINACT", "onDestroy!");
        super.onDestroy();

    }
}
