package com.sathya.plrsmartwatchapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sathya.plrsmartwatchapp.R;
import com.sathya.plrsmartwatchapp.apis.RestClient;
import com.sathya.plrsmartwatchapp.models.GetUserStateRes;
import com.sathya.plrsmartwatchapp.utils.Constants;
import com.sathya.plrsmartwatchapp.utils.PrefHelper;

import retrofit2.Call;
import retrofit2.Response;

public class DashActivity extends BaseActivity {

    private TextView tvTitle;
    ImageView ivHelp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash);

        callGetUserAPI();

        tvTitle = findViewById(R.id.tvTitle);
        ivHelp = (ImageView) findViewById(R.id.ivHelp);

        ivHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashActivity.this, SendMessageActivity.class);
                startActivity(intent);
            }
        });

        // Enables Always-on
        setAmbientEnabled();
    }

    private void callGetUserAPI() {
        Call<GetUserStateRes> objectCall = RestClient.getApiClient().getUserState();
        restClient.makeApiRequest(DashActivity.this, objectCall, this, Constants.REQ_CODE_GET_USER_STATE, false);
    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode) {
        switch (reqCode) {
            case Constants.REQ_CODE_GET_USER_STATE:
                GetUserStateRes getUserStateRes = (GetUserStateRes) response.body();
                PrefHelper.setBoolean(Constants.IS_PAID_USER, getUserStateRes.getData().isSubscription());
                break;
        }
    }

    @Override
    public void onApiError(Call<Object> call, Object object, int reqCode) {
        super.onApiError(call, object, reqCode);
        switch (reqCode) {
            case Constants.REQ_CODE_GET_MY_COURSES_LIST:
                Toast.makeText(DashActivity.this, "" + object.toString(), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(DashActivity.this, "" + object.toString(), Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
