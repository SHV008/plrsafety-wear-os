package com.sathya.plrsmartwatchapp.activities;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.sathya.plrsmartwatchapp.R;
import com.sathya.plrsmartwatchapp.utils.Constants;
import com.sathya.plrsmartwatchapp.utils.PrefHelper;
import com.sathya.plrsmartwatchapp.utils.Utils;

import java.util.List;
import java.util.Locale;

public class SendMessageActivity extends WearableActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private TextView tvTitle;
    private TextView tvLocation;
    private Button ivSend;

    //Define a request code to send to Google Play services
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude = 0.0;
    private double currentLongitude = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);

        initialiseMap();

        tvLocation = findViewById(R.id.tvLocation);
        ivSend = findViewById(R.id.ivSend);

        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(SendMessageActivity.this, "Send Message", Toast.LENGTH_SHORT).show();
                if (PrefHelper.getBoolean(Constants.IS_PAID_USER, false)) {
                    if (Utils.getInstance().checkInternetConnection(SendMessageActivity.this)) {
                        if (PrefHelper.getBoolean(Constants.IS_LOGIN, false)) {
                            //if (PrefHelper.getBoolean(Constants.IS_SEND_MESSAGE, false)) {
                            String senderNumber = PrefHelper.getString(Constants.COUNTRY_CODE, "") + PrefHelper.getString(Constants.PRIMARY_EMERGENCY_NUMBER, "");
                            if (currentLatitude != 0.0 && currentLongitude != 0.0) {
                                sendSMS("" + senderNumber, "PLR : Address - " + getCompleteAddressString(currentLatitude, currentLongitude) + ", http://maps.google.com/?q=" + String.format("%.4f", currentLatitude) + "," + String.format("%.4f", currentLongitude) + " " + PrefHelper.getString(Constants.FULL_NAME, "") + " " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, ""));
                            } else {
                                sendSMS("" + senderNumber, "PLR : Name - " + PrefHelper.getString(Constants.FULL_NAME, "") + ", Number - " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, ""));
                            }
                        }
                    } else {
                        Toast.makeText(SendMessageActivity.this, "Network is not available so once network is available then it will be sent message automatically", Toast.LENGTH_LONG).show();
                        String message = "";
                        if (currentLatitude != 0.0 && currentLongitude != 0.0) {
                            message = "PLR : Address - " + getCompleteAddressString(currentLatitude, currentLongitude) + ", Lat : " + currentLatitude + "  Long : " + currentLongitude + " " + PrefHelper.getString(Constants.FULL_NAME, "") + " " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, "");
                        } else {
                            message = "PLR : Name - " + PrefHelper.getString(Constants.FULL_NAME, "") + ", Number - " + PrefHelper.getString(Constants.PERSONAL_MOBILE_NUMBER, "");
                        }

                        PrefHelper.setString(Constants.STORED_MESSAGE, "" + message);
                    }
                } else {
                    Toast.makeText(SendMessageActivity.this, "You have to subscribe packages to send message", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Enables Always-on
        setAmbientEnabled();
    }

    public void initialiseMap() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds
    }

    public void sendSMS(String phoneNo, String msg) {
        try {
            /*SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);*/

            startActivity(new Intent(SendMessageActivity.this, ImmediateAssistanceActivity.class)
                    .putExtra("PhoneNo", phoneNo)
                    .putExtra("SendMessage", msg));
        } catch (Exception ex) {
            Toast.makeText(SendMessageActivity.this, ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Now lets connect to the API
        mGoogleApiClient.connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");

        //Disconnect from API onPause()
        try {
            if (mGoogleApiClient != null) {
                if (mGoogleApiClient.isConnected()) {
                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, (com.google.android.gms.location.LocationListener) this);
                    mGoogleApiClient.disconnect();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * If connected get lat and long
     */
    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) this);

        } else {
            //If everything went fine lets get latitude and longitude
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            String strAddress = getCompleteAddressString(currentLatitude, currentLongitude);
            tvLocation.setText(strAddress);
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    /**
     * If locationChanges change lat and long
     *
     * @param location
     */
    @Override
    public void onLocationChanged(Location location) {
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();

        String strAddress = getCompleteAddressString(currentLatitude, currentLongitude);
        tvLocation.setText(strAddress);
        //Toast.makeText(this, currentLatitude + " : " + currentLongitude + "", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                //Log.w("My Current loction address", strReturnedAddress.toString());
            } else {
                //Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current address", "Canont get Address!");
        }
        return strAdd;
    }

}
