package com.sathya.plrsmartwatchapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.wearable.activity.WearableActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sathya.plrsmartwatchapp.R;
import com.sathya.plrsmartwatchapp.utils.Constants;
import com.sathya.plrsmartwatchapp.utils.PrefHelper;
import com.sathya.plrsmartwatchapp.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImmediateAssistanceActivity extends WearableActivity {

    @BindView(R.id.tvCount)
    TextView tvCount;
    @BindView(R.id.btnStop)
    Button btnStop;
    @BindView(R.id.llSent)
    LinearLayout llSent;

    private int count = 0, highCount = 5;
    private boolean isStop = false;

    private String strPhoneNo = "", strSendMessage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assistance);
        ButterKnife.bind(this);

        initialiseViews();
    }

    public void initialiseViews() {
        if (getIntent() != null) {
            strPhoneNo = getIntent().getExtras().getString("PhoneNo");
            strSendMessage = getIntent().getExtras().getString("SendMessage");
        }

        count = 1;
        tvCount.setVisibility(View.VISIBLE);
        btnStop.setVisibility(View.VISIBLE);
        tvCount.setText("" + highCount);
        RunAnimation();
        animateCount();
        isStop = false;
    }

    private void RunAnimation() {
        Animation a = AnimationUtils.loadAnimation(this, R.anim.scale);
        a.reset();
        tvCount.clearAnimation();
        tvCount.startAnimation(a);
    }

    public void animateCount() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!isStop) {
                    if (highCount != 0) {
                        highCount = highCount - count;
                        tvCount.setText("" + highCount);
                        animateCount();
                        RunAnimation();
                    } else if (highCount == 0) {
                        if (PrefHelper.getBoolean(Constants.IS_PAID_USER, false)) {
                            if (Utils.getInstance().checkInternetConnection(ImmediateAssistanceActivity.this)) {
                                if (PrefHelper.getBoolean(Constants.IS_LOGIN, false)) {
                                    sendSMS(strPhoneNo, strSendMessage);
                                }
                            }
                        } else {
                            Toast.makeText(ImmediateAssistanceActivity.this, "You have to subscribe packages to send message", Toast.LENGTH_SHORT).show();
                        }
                        llSent.setVisibility(View.VISIBLE);
                        tvCount.setVisibility(View.GONE);
                        btnStop.setVisibility(View.GONE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                llSent.setVisibility(View.GONE);
                                highCount = 5;
                                startActivity(new Intent(ImmediateAssistanceActivity.this, DashActivity.class));
                                finish();
                            }
                        }, 3000);
                    }
                }
            }
        }, 800);
    }

    @OnClick(R.id.btnStop)
    public void onViewClicked() {
        isStop = true;
        tvCount.setVisibility(View.GONE);
        btnStop.setVisibility(View.GONE);
        tvCount.clearAnimation();
        highCount = 5;
        finish();
    }

    public void sendSMS(String phoneNo, String msg) {
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
        } catch (Exception ex) {
            Toast.makeText(ImmediateAssistanceActivity.this, ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }
}
