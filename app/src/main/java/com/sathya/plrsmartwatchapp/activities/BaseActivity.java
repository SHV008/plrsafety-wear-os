package com.sathya.plrsmartwatchapp.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.Toast;

import com.sathya.plrsmartwatchapp.IdealDistributorApp;
import com.sathya.plrsmartwatchapp.apis.ApiResponseListener;
import com.sathya.plrsmartwatchapp.apis.RestClient;
import com.sathya.plrsmartwatchapp.interfaces.BackPressedEventListener;
import com.sathya.plrsmartwatchapp.interfaces.ClickEventListener;

import retrofit2.Call;
import retrofit2.Response;


public class BaseActivity extends WearableActivity implements BackPressedEventListener, ClickEventListener, ApiResponseListener {
    private static final String TAG = "BaseActivity";
    private Toast toast;

    public RestClient restClient;
    public IdealDistributorApp appInstance;

    protected SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appInstance = (IdealDistributorApp) getApplication();
        restClient = appInstance.getRestClient();
        //Fabric.with(this, new Crashlytics());
    }

    public IdealDistributorApp getApp() {
        return (IdealDistributorApp) this.getApplication();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void clickEvent(View v) {

    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode) {

    }

    @Override
    public void onApiResponse(Call<Object> call, Response<Object> response, int reqCode, int position) {

    }

    @Override
    public void onApiError(Call<Object> call, Object object, int reqCode) {

    }

    @Override
    public void onDataNotFound(Call<Object> call, Object object, int reqCode) {

    }
}
