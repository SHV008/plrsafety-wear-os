package com.sathya.plrsmartwatchapp.interfaces;

/**
 * Created by Shailesh on 1/28/2017.
 */

public interface DialogActionClickListener {
    public void onDialogClicked(int code, long amount, boolean isValid, long remainingAmount);

    public void onDialogClicked();
}
