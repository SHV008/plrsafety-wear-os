package com.sathya.plrsmartwatchapp.interfaces;

/**
 * Created by ip4 on 15-06-2016.
 */
import android.view.View;

public interface ClickEventListener {
    public void clickEvent(View v);
}
