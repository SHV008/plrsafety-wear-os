package com.sathya.plrsmartwatchapp.interfaces;


public interface BackPressedEventListener {
	public void onBackPressed();
}
